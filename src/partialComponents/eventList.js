import React, { useContext, useRef, useState } from "react"
import dayjs from "dayjs"
import GlobalContext from "../context/globalContext"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSearch } from "@fortawesome/free-solid-svg-icons"

export default function EventList() {
    const { eventList, updateEventList, savedEvents, setShowEventModal, setSelectedEvent, searchTerm, setSearchTerm } = useContext(GlobalContext)
    const [searchResult, setSearchResult] = useState(savedEvents)
    const inputEl = useRef("")
    function handleEventModal() {
        setShowEventModal(true)
    }

    function handleSearch(e) {
        let query = inputEl.current.value
        setSearchTerm(inputEl.current.value)
        if (searchTerm !== "") {
            const newSearchResult = savedEvents.filter(evt => evt.title.toLowerCase() === query.toLowerCase())
            setSearchResult(newSearchResult)
        } else {
            setSearchResult(savedEvents)
        }
    }



    return (
        <React.Fragment>
            <p className="text-gray-500 font-bold mt-10">
                Event List
            </p>
            <div>
                <input type='text' placeholder="Search event" name="search" 
                    className="border-0 text-gray-600 border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500"
                    ref={inputEl}
                    onChange={(e) => handleSearch(e)}
                />
                <FontAwesomeIcon icon={faSearch} className="w-5 h-5 mt-5 my-auto absolute" />
            </div>
            {
                searchTerm.length > 1 ?
                    searchResult.map((evt, i) =>(
                        <div className={`${evt.status === 'PENDING' ? 'bg-red-200' : evt.status === 'DONE' ? 'bg-green-200' : evt.status === 'ON-GOING' ? 'bg-yellow-200' : ''} 
                            py-2 px-1 my-3 rounded cursor-pointer block`}
                            key={i}
                            onClick={() => {
                                handleEventModal()
                                setSelectedEvent(evt)
                            }}
                        >
                            <h5 className="items-center mt-3" key={i}>
                                {evt.title}
                            </h5>
                            <small>{dayjs(evt.day).format('dddd, MMMM DD')}</small>
                        </div>
                    ))
                :
                savedEvents.map((evt, i) =>(
                    <div className={`${evt.status === 'PENDING' ? 'bg-red-200' : evt.status === 'DONE' ? 'bg-green-200' : evt.status === 'ON-GOING' ? 'bg-yellow-200' : ''} 
                        py-2 px-1 my-3 rounded cursor-pointer block`}
                        key={i}
                        onClick={() => {
                            handleEventModal()
                            setSelectedEvent(evt)
                        }}
                    >
                        <h5 className="items-center mt-3" key={i}>
                            {evt.title}
                        </h5>
                        <small>{dayjs(evt.day).format('dddd, MMMM DD')}</small>
                    </div>
                ))

            }
            <p className="text-gray-500 font-bold mt-10">
                Status
            </p>
            {
                eventList.map(({status: stat, checked}, idx) => (
                    <label className="items-center mt-3 block" key={idx}>
                        <input type="checkbox" checked={checked} className={`${stat === 'PENDING' ? 'text-red-400' : stat === 'DONE' ? 'text-green-400' : stat === 'ON-GOING' ? 'text-yellow-400' : ''} 
                            form-checkbox h-5 w-5  rounded focus:ring-0 cursor-pointer`}
                            onChange={() => updateEventList({status: stat, checked: !checked})}
                        />
                        <span className="ml-2 text-gray-700 capitalized">{stat}</span>
                    </label>
                ))
            }
        </React.Fragment>
    )
}
