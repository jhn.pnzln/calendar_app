import dayjs from "dayjs"
import React, { useState, useEffect, useContext } from "react"
import { getMonth } from '../helper'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons"

import GlobalContext from '../context/globalContext'

export default function SmallCalendar() {
    const [currentMonthIdx, setCurrentMonthIdx] = useState(dayjs().month())
    const [currentMonth, setCurrentMonth] = useState(getMonth())

    useEffect(() => {
        setCurrentMonth(getMonth(currentMonthIdx))
    }, [currentMonthIdx]);

    const { monthIndex, setSmallCalendarMonth, daySelected, setDaySelected } = useContext(GlobalContext)

    useEffect(() => {
        setCurrentMonthIdx(monthIndex)
    }, [monthIndex])

    function handlePrevMonth() {
        setCurrentMonthIdx(currentMonthIdx - 1)
    }

    function handleNextMonth() {
        setCurrentMonthIdx(currentMonthIdx + 1)
    }

    function getDayClass(day) {
        const format = 'DD-MM-YYY'
        const nowDay = dayjs().format(format)
        const currDay = day.format(format)
        const selectedDay = daySelected && daySelected.format(format)
        if (nowDay === currDay) {
            return 'bg-blue-500 rounded-full text-white'
        } else if (currDay === selectedDay) {
            return 'bg-blue-100 rounded-full text-blue-600 font-bold'
        } else {
            return null
        }
    }

    return (
        <div className="mt-9 ">
            <header className="flex justify-between">
                <p className="text-gray-500 font-bold">
                    {dayjs(new Date(dayjs().year(), currentMonthIdx)).format('MMMM YYYY')}
                </p>
                <div>
                    <button onClick={handlePrevMonth}>
                        <span className="material-icons-outlined cursor-pointer text-gray-500 mx-2">
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </span>
                    </button>
                    <button onClick={handleNextMonth}>
                        <span className="material-icons-outlined cursor-pointer text-gray-500 mx-2">
                            <FontAwesomeIcon icon={faChevronRight} />
                        </span>
                    </button>
                </div>
            </header>
            <div className="grid grid-cols-7 grid-rows-6">
                {currentMonth[0].map((day, i) => (
                    <span className="text-sm py-1 text-center" key={i}>
                        {day.format('dd').charAt(0)}
                    </span>
                ))}
                {currentMonth.map((row, i) =>(
                    <React.Fragment key={i}>
                        {row.map((day, i) => (
                            <button className={`py-1 w-full ${getDayClass(day)}`} key={i} 
                                onClick={() => {
                                    setSmallCalendarMonth(currentMonthIdx);
                                    setDaySelected(day)
                                }}
                            >
                                <span className="text-sm">{day.format('D')}</span>
                            </button>
                        ))}
                    </React.Fragment>
                ))}
            </div>
        </div>
    )
}
