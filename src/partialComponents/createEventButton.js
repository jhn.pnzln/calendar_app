import React, { useContext } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import GlobalContext from "../context/globalContext"

export default function CreateEventButton() {
    const {setShowEventModal} = useContext(GlobalContext)

    return <button className="border p-2 rounded-full items-center shadow-md hover:shadow-2xl"
                onClick={() => setShowEventModal(true)}
            > 
        <FontAwesomeIcon icon={faPlus} className="w-7 h-7" />
        <span className="pl-3 pr-7">Create</span>
    </button>
}
