import React, { useContext, useState } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAlignJustify, faClock, faTimes, faAlignRight, faTrash, faSave } from "@fortawesome/free-solid-svg-icons"
import GlobalContext from '../context/globalContext'

const status = ["PENDING", "DONE", "ON-GOING"]

export default function EventModal() {
    const {setShowEventModal, daySelected, dispatchCalendarEvent, selectedEvent} = useContext(GlobalContext)
    const [title, setTitle] = useState(selectedEvent ? selectedEvent.title : '')
    const [selectedStatus, setStatus] = useState(selectedEvent ? status.find((stat) => stat === selectedEvent.status) : status[0])
    
    function handleSubmit(e) {
        e.preventDefault()
        const calendarEvent = {
            title,
            status: selectedStatus,
            day: daySelected.valueOf(),
            id: selectedEvent ? selectedEvent.id : Date.now()
        }
        console.log('_____PAYLOAD', calendarEvent)
        if (selectedEvent) {
            dispatchCalendarEvent({type: 'update', payload: calendarEvent})
        } else {
            dispatchCalendarEvent({type: 'push', payload: calendarEvent})
        }
        
        setShowEventModal(false)
    }

    return (
        <div className='h-screen w-full fixed left-0 top-0 flex justify-center items-center'>
            <form className='bg-white rounded-lg shadow-2xl w-1/4'>
                <header className='bg-gray-100 px-4 py-2 flex justify-between items-center'>
                    <span className='material-icons-ountlined text-gray-400'>
                        <FontAwesomeIcon icon={faAlignJustify} />
                    </span>
                    <div>
                        <button onClick={() => setShowEventModal(false)}>
                            <FontAwesomeIcon icon={faTimes} className='text-gray-400' />
                        </button>
                    </div>
                </header>
                <div className='p-3'>
                    <div className='grid grid-cols-1/5 items-end gap-y-7'>
                        <div></div>
                        <input type="text" 
                            name='title' 
                            placeholder='Add title' 
                            value={title}
                            required
                            className='pt-3 border-0 text-gray-600 text-xl font-semibold pb-2 w-full border-b-2 border-gray-200 focus:outline-none focus:ring-0 focus:border-blue-500'
                            onChange={(e) => setTitle(e.target.value)}
                        />
                        <FontAwesomeIcon icon={faClock} />
                        <p>{daySelected.format('dddd, MMMM DD')}</p>
                        <FontAwesomeIcon icon={faAlignRight} />
                        <select className='w-full h-10 pl-3 pr-6 text-base placeholder-gray-600 border rounded-lg appearance-none focus:shadow-outline" placeholder="Regular input'
                            onChange={(e) => setStatus(e.target.value)}
                        >
                            {
                                status.map((stat, i) =>(
                                    <option key={i} value={stat} selected={selectedEvent ? stat == selectedEvent.status : null}>{stat}</option>
                                ))
                            }
                        </select>
                    </div>
                </div>
                <footer className='flex justify-evenly border-t p-3 mt-5'>
                    {
                        selectedEvent && (
                            <button type='submit' className='bg-red-500 hover:bg-red-600 px-6 py-2 rounded text-white'
                                onClick={() => {
                                    dispatchCalendarEvent({type: 'delete', payload: selectedEvent});
                                    setShowEventModal(false)
                                }}
                            >
                                <FontAwesomeIcon icon={faTrash} /> Delete
                            </button>
                        )
                    }
                    
                    <button type='submit' className='bg-blue-500 hover:bg-blue-600 px-6 py-2 rounded text-white'
                        onClick={(e) => handleSubmit(e)}
                    >
                        <FontAwesomeIcon icon={faSave} /> { selectedEvent ? 'Update' : 'Save'}
                    </button>
                </footer>
            </form>
        </div>
    )
}
