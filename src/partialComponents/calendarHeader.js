import React, {useContext} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import GlobalContext from '../context/globalContext'
import dayjs from 'dayjs'


export default function CalendarHeader() {
    const { monthIndex, setMonthIndex } = useContext(GlobalContext)

    function handlePrevMonth() {
        setMonthIndex(monthIndex - 1)
    }

    function handleNextMonth() {
        setMonthIndex(monthIndex + 1)
    }

    function handleReset() {
        setMonthIndex(monthIndex === dayjs().month() ? monthIndex + Math.random() : dayjs().month())
    }

    return (
        <header className='px-4 py-2 flex items-center'>
            {/* <img src='' alt='calender' className='mr-2 w-12 h-12'/> */}
            <h1 className='mr-10 text-xl text-gray-500 font-bold'> Calendar App</h1>
            <button className='border rounded py-2 px-4 mr-5' onClick={handleReset}>
                Today
            </button>
            <button onClick={handlePrevMonth}>
                <span className='material-icons-outlined cursor-pointer text-gray-600 mx-2'>
                    <FontAwesomeIcon icon={faChevronLeft} />
                </span>
            </button>
            <button onClick={handleNextMonth}>
                <span className='material-icons-outlined cursor-pointer text-gray-600 mx-2'>
                    <FontAwesomeIcon icon={faChevronRight} />
                </span>
            </button>
            <h2 className='ml-4 text-xl text-gray-500 font-bold'>
                {dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY')}
            </h2>
        </header>
    )
}