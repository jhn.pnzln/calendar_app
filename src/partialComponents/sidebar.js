import React from 'react'
import CreateEventButton from './createEventButton'
import EventList from './eventList'
import SmallCalendar from './smallCalendar'

export default function Sidebar() {
    return (
        <aside className='border p-5 w-64'>
            <CreateEventButton />
            <SmallCalendar />
            <EventList />
        </aside>
    )
}