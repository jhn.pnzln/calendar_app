import React, { useState, useContext, useEffect } from 'react';
import './index.css';
import { getMonth } from './helper'
import CalendarHeader from './partialComponents/calendarHeader';
import Sidebar from './partialComponents/sidebar';
import Month from './partialComponents/month';
import GlobalContext from './context/globalContext';
import EventModal from './partialComponents/eventModal';

function App() {
  const [currentMonth, setCurrentMonth] = useState(getMonth())
  const { monthIndex, showEventModal } = useContext(GlobalContext)

  useEffect(() => {
    setCurrentMonth(getMonth(monthIndex))
  },  [monthIndex])

  return (
    <div className="App">
      { showEventModal && <EventModal/>}
      <div className='h-screen flex flex-col'>
        <CalendarHeader />
        <div className='flex flex-1'>
          <Sidebar/>
          <Month month={currentMonth}/>
        </div>
      </div>
    </div>
  );
}

export default App;
