import React, { useEffect, useMemo, useReducer, useState } from 'react'
import GlobalContext from './globalContext'
import dayjs, { Dayjs } from 'dayjs'

function savedEventReducer(state, {type, payload}) {
    switch (type) {
        case 'push':
            return [...state, payload];
        case 'update':
            return state.map(evt => evt.id === payload.id ? payload : evt);
        case 'delete':
            return state.filter(evt => evt.id !== payload.id);
        default:
            throw new Error();
    }
}

function initEvents() {
    const storageEvents = localStorage.getItem('savedEvents')
    const parsedEvents = storageEvents ? JSON.parse(storageEvents) : []
    return parsedEvents
}

export default function ContextWrapper(props) {
    const [monthIndex, setMonthIndex] = useState(dayjs().month())
    const [smallCalenderMonth, setSmallCalendarMonth] = useState(null)
    const [daySelected, setDaySelected] = useState(dayjs())
    const [showEventModal, setShowEventModal] = useState(false)
    const [selectedEvent, setSelectedEvent] = useState(null)
    const [eventList, setEventList] = useState([])
    const [savedEvents, dispatchCalendarEvent] = useReducer(savedEventReducer, [], initEvents)
    const [searchTerm, setSearchTerm] = useState("")

    const filteredEvents = useMemo(() => {
        return savedEvents.filter((evt) => 
            eventList
            .filter(stat => stat.checked)
            .map(stat => stat.status)
            .includes(evt.status)
        )
    },[savedEvents, eventList])

    useEffect(() => {
        localStorage.setItem('savedEvents', JSON.stringify(savedEvents))
    }, [savedEvents])

    useEffect(() => {
        if(smallCalenderMonth !== null) {
            setMonthIndex(smallCalenderMonth)
        }
    }, [smallCalenderMonth])

    function updateEventList(evt) {
        setEventList(eventList.map((stat) => stat.status === evt.status ? evt : stat))
    }

    useEffect(() => {
        setEventList((prevStatus) => {
            return [...new Set(savedEvents.map(evt => evt.status))].map(status => {
                const currentStatus = prevStatus.find(stat => stat.status === status)
                return {
                    status,
                    checked: currentStatus ? currentStatus.checked : true
                }
            })
        })
    }, [savedEvents])

    useEffect(() => {
        if(!showEventModal) {
            setSelectedEvent(null)
        }
    }, [showEventModal])

    return (
        <GlobalContext.Provider 
            value={{ 
                monthIndex, 
                setMonthIndex, 
                smallCalenderMonth, 
                setSmallCalendarMonth,
                daySelected,
                setDaySelected,
                showEventModal,
                setShowEventModal,
                dispatchCalendarEvent,
                savedEvents,
                selectedEvent,
                setSelectedEvent,
                setEventList,
                eventList,
                updateEventList,
                filteredEvents,
                searchTerm,
                setSearchTerm,
            }}
        >
            {props.children}
        </GlobalContext.Provider>
    )
}
